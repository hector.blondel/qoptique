def verification (fusions, blocs_de_qubits):##Un bloc de qubits est une liste d'arrêtes
    for couple in fusions:
        bloc_retenu_1, bloc_retenu_2 = [],[]
        qubit1, qubit2 = couple[0],couple[1]
        for bloc1 in blocs_de_qubits:
            for bloc2 in blocs_de_qubits:
                for arrête_1 in bloc1:
                    for arrête_2 in bloc2:
                        if qubit1 in arrête_1:
                            bloc_retenu_1 = bloc1
                            if qubit2 in bloc2:
                                bloc_retenu_2 = bloc2##on peut ajouter un arrêt une fois les blocs trouvés
        if bloc_retenu_1 == bloc_retenu_2:
            bloc_fusionné = bloc_retenu_1
            for arrête1 in bloc_retenu_1:
                for arrête2 in bloc_retenu_2:
                    if arrête1[0] == qubit1 and arrête2[0] == qubit2:
                        bloc_fusionné.append([arrête1[1], arrête2[1]])
                    if arrête1[0] == qubit1 and arrête2[1] == qubit2:
                        bloc_fusionné.append([arrête1[1], arrête2[0]])
                    if arrête1[1] == qubit1 and arrête2[1] == qubit2:
                        bloc_fusionné.append([arrête1[0], arrête2[0]])

                    if arrête1[0] == qubit1 and arrête2[1] == qubit2:
                        bloc_fusionné.append([arrête1[1], arrête2[0]])

                    if arrête1[0] == qubit1 or arrête1[1] == qubit1:
                        bloc_fusionné.remove(arrête1)
                    if arrête2[0] == qubit2 or arrête2[1] == qubit2:
                        bloc_fusionné.remove(arrête2)
        

            blocs_de_qubits.remove(bloc_retenu_1)
            blocs_de_qubits.append(bloc_fusionné)
            
        else:
            bloc_fusionné = bloc_retenu_1
            for arrête1 in bloc_retenu_1:
                for arrête2 in bloc_retenu_2:
                    if arrête1[0] == qubit1 and arrête2[0] == qubit2:
                        bloc_fusionné.append([arrête1[1], arrête2[1]])
                    if arrête1[0] == qubit1 and arrête2[1] == qubit2:
                        bloc_fusionné.append([arrête1[1], arrête2[0]])
                    if arrête1[1] == qubit1 and arrête2[0] == qubit2:
                        bloc_fusionné.append([arrête1[0], arrête2[1]])
                    if arrête1[1] == qubit1 and arrête2[1] == qubit2:
                        bloc_fusionné.append([arrête1[0], arrête2[0]])

                    if arrête1[0] == qubit1 or arrête1[1] == qubit1:
                        bloc_fusionné.remove(arrête1)
                    if arrête2[0] == qubit2 or arrête2[1] == qubit2:
                        bloc_fusionné.remove(arrête2)
        

            blocs_de_qubits.remove(bloc_retenu_1)
            blocs_de_qubits.remove(bloc_retenu_2)
            blocs_de_qubits.append(bloc_fusionné)
    return blocs_de_qubits##il n'y a plus que les arrêtes du graphe



blocs_de_qubits = [['qubit00','qubit01'],['qubit10', 'qubit11']]
fusions = [['qubit01','qubit11']]

print(verification(fusions, blocs_de_qubits))
