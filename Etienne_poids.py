def affectation_de_poids(G):
    V,E= G
    liste_des_nombres_de_voisins = V
    for i in range(len(V)) :
        liste_des_nombres_de_voisins[i] = nombre_de_voisins(G,i)
    poids = E
    for i in range(len(E)):
        poids[i] = nombre_de_voisins(E[i][0]) + nombre_de_voisins(E[i][1])
    return poids


def nombre_de_voisins(G,i):
    V,E = G
    résultat = 0
    sommet_considéré = V[i]
    for arrête in E:
        if arrête[0] == sommet_considéré:
            résultat+=1
        if arrête[1] == sommet_considéré:
            résultat+=1
    return résultat

from heapq import heappush, heappop


def prim(edges, nodes):
    """Ce code utilise une file de priorité pour stocker les arêtes à considérer, implémentée par les fonctions heappush et heappop de la bibliothèque heapq. 
    Chaque arête est représentée par un tuple de la forme (poids, sommet1, sommet2), et la liste des arêtes est passée en argument à la fonction prim. 
    La fonction retourne l'ensemble des arêtes sélectionnées pour construire l'arbre couvrant de poids minimum.
    """
    mst = []
    visited = set()
    heap = [(0, next(iter(nodes)))]
    
    while heap:
        weight, node = heappop(heap)
        if node in visited:
            continue
        visited.add(node)
        mst.append((weight, node))
        
        for edge in edges:
            if node in edge:
                neighbor = edge[0] if edge[1] == node else edge[1]
                if neighbor not in visited:
                    heappush(heap, (edge[2], neighbor))
                    
    return mst

def f(G):
    fusions = []
    chaînes_à_produire = []##[[qb00,qb01],[qb10,qb11,qb12]]par exemple
    E,V = G
    mst = prim(E,V)
    mst_light = [[mst[i][1],mst[i][2]] for i in range len(mst)]
    global arrêtes_restantes
    arrêtes_restantes = soustraction(E,mst_light)
    for i in range(len(mst)):
        sommet = mst[i][1]
        chaînon_à_greffer = construction_chaînon(arrêtes_restantes,sommet,i)
        chaînes_à_produire.append(chaînon_à_greffer)
        fusions.append([sommet, chaînon_à_greffer[0]])


        sommet = mst[i][2]
        chaînon_à_greffer = construction_chaînon(arrêtes_restantes,sommet,i)
        chaînes_à_produire.append(chaînon_à_greffer)
        fusions.append([sommet, chaînon_à_greffer[0]])

    for i in range(len(mst)):
        sommet = mst_light[i][0]
        vertexes_de_mst = vertexes(mst_light)
        if nombre_de_voisins((vertexes_de_mst,mst_light),sommet)>=2
    
    

    return fusions, chaînes_à_produire


def vertexes(mst_light):
    resu=[]
    for i in range(len(mst_light)):
        if mst_light[i][0] not in resu:
            resu.append(mst_light[i][0])
        if mst_light[i][1] not in resu:
            resu.append(mst_light[i][1])

def soustraction (E1,E2):
    resu = []
    for arrête in E1:
        if arrête not in E2:
            resu.add(arrête)

def construction_chaînon(arrêtes_restantes,sommet, i):
    chaînon = []
    for i in range(len(arrêtes_restantes)):
        if arrêtes_restantes[i][0]==sommet:
            chaînon.append(qb)
            arrêtes_restantes.pop(i)