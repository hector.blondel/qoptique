#coding:utf-8

import numpy as np
import copy
from constructiongraphe import random_graph_generator
from simplification import simplification


# On se donne un graphe représenté par ses listes d'adjacence. Les sommets sont indexés par des indices :
# Le résultat final prend la forme d'une liste [] de taille
# le nombre de chaînes que l'on veut fusionner et chaque élément est la taille d'une chaîne.
# et d'une liste de fusions [(numero_element1,indice_element1)]


n_nodes = 6
graphe1 = random_graph_generator(n_nodes)
graphe = graphe1[0] #entrée : listes d'adjacence
#graphe = [[1,2],[0,2,3],[0,1],[1]]
graphe = [[1,2],[0,2,3],[0,1,3],[1,2]]
#graphe = [[2, 4], [3, 5], [0, 4, 5], [1, 4], [0, 2, 3, 5], [1, 2, 4]]
graphe = [[1,3,5],[1,3,5],[1,3,5],[0,2,4],[0,2,4],[0,2,4]]


def algorithme_decomposition(graphe,methode='largeur',simp=True):
    taillegraphe = len(graphe) #nb de sommets



    longueurchaines = [1] #sortie 1 : liste des longueurs des chaînes
    fusions = [] #sortie 2 : liste des opérations de fusion, au format a,b,c,d avec a le no de la 1re chaîne, b la place dans cette chaîne, c le no de la 2de chaîne, et d la place dans cette 2de chaîne


    sommetsatraiter = [0]
    #prédécesseurs = [[]]*taillegraphe

    sommetsetchaines = [(0,0)]+[(None,None)]*(taillegraphe-1) #Associe à chaque sommet le numéro de sa chaîne et à sa place dans la chaîne



    esttraite = [False]*taillegraphe


    nbsommetstraites = 0

    findechaine = True

    def prochainsommet():
        if nbsommetstraites == taillegraphe :
            return None
        elif len(sommetsatraiter) > 0:
            if methode == "largeur":
                return sommetsatraiter.pop(0)
            if methode == "profondeur":
                return sommetsatraiter.pop()
        else:
            for i in range(taillegraphe):
                if i not in esttraite[i]:
                    return i


    def fusion4(s1,s2):
        chainedeliaison = len(longueurchaines)
        longueurchaines.append(4)
        a,b = sommetsetchaines[s1]
        c,d = sommetsetchaines[s2]
        fusions.append(((a,b),(chainedeliaison,0)))
        fusions.append(((c,d),(chainedeliaison,3)))
        sommetsetchaines[s1]=chainedeliaison,1
        sommetsetchaines[s2]=chainedeliaison,2

    def fusion3(s1,s2):
        chainedeliaison = len(longueurchaines) # On ajoute une chaîne de liaison de longueur 4 à la fin de la liste des chaînes.
        longueurchaines.append(3)
        a,b = sommetsetchaines[s1] # sommet courant en pratique
        fusions.append(((a,b),(chainedeliaison,0)))
        sommetsetchaines[s1] = chainedeliaison,1
        sommetsetchaines[s2] = chainedeliaison,2

    while nbsommetstraites != taillegraphe :
        if nbsommetstraites != taillegraphe :
            sommetcourant = prochainsommet()
        # print('sommetstraites=',esttraite)
        # print('sommetsatraiter=',sommetsatraiter)
        # print('sommetcourant=',sommetcourant)
        # print('fusions=',fusions)
        # print('longueurchaines=',longueurchaines)
        esttraite[sommetcourant] = True
        nbsommetstraites += 1
        listevoisins = copy.deepcopy(graphe[sommetcourant])
        # print('listevoisins1=',listevoisins)
        # for i in range(len(listevoisins)):
        #     print(i)
        #     voisin = listevoisins[i]
        #     if esttraite[voisin]:
        #         listevoisins.remove(voisin)

        voisinsasupprimer = []
        listevoisins = [voisin for voisin in listevoisins if not esttraite[voisin]]
        #On a supprimé de listevoisins tous mes voisins qui ont déjà été traités (car sinon la liaison vers ces voisins a aussi déjà été traitée)
        # print('listevoisins=',listevoisins)
        nbvoisins = len(listevoisins)
        # print('nbvoisins=',nbvoisins)
        chainecourante, placechaine = sommetsetchaines[sommetcourant]
        findechaine = bool(placechaine == (longueurchaines[chainecourante]-1))
        if nbvoisins == 0:
            pass
            # Le sommet courant a déjà été ajouté à une chaîne : il n'y a plus rien à faire.
        elif nbvoisins == 1:
            uniquevoisin = listevoisins[0]
            if uniquevoisin not in sommetsatraiter:
                sommetsatraiter.append(uniquevoisin)
                if findechaine :
                    sommetsetchaines[uniquevoisin] = (chainecourante,placechaine+1)
                    longueurchaines[chainecourante]+=1
                else:
                    fusion3(sommetcourant, uniquevoisin)
            else:
                fusion4(sommetcourant, uniquevoisin)
        else:
            voisinlinéaire = False # Si l'un des voisins n'a pas été traité, on l'ajoute simplement à la chaîne du sommet actuel (on ne fait ça que pour un sommet).
            if not findechaine:
                voisinlinéaire = True
            for i in range(nbvoisins):
                voisini = listevoisins[i]
                if voisini not in sommetsatraiter and not voisinlinéaire:
                    voisinlinéaire = True
                    longueurchaines[chainecourante]+=1
                    sommetsetchaines[voisini] = (chainecourante,placechaine+1)
                    sommetsatraiter.append(voisini)
                elif voisini not in sommetsatraiter:
                        sommetsatraiter.append(voisini)
                        fusion3(sommetcourant,voisini)
                else :
                    fusion4(sommetcourant,voisini)

        # print('sommetstraites=',esttraite)
        # print('sommetsatraiter=',sommetsatraiter)
        # print('sommetcourant=',sommetcourant)


    if simp :
        longueurchaines,fusions = simplification(longueurchaines,fusions)
    return longueurchaines,fusions

longueurchaines, fusions = algorithme_decomposition(graphe,methode='largeur',simp=True)

print('graphe : ',graphe)
print('chaînes : ',longueurchaines)
print('fusions : ',fusions)