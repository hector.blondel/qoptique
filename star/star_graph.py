import sys
import os
import numpy as np
import matplotlib.pyplot as plt

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

import constructiongraphe

class Graph:
    """graph1 is the graph without the double arrows
    graph2 is the one with the double arrows"""
    def __init__(self,graphstate):
        self.peripheral_graph = graphstate
        self.central_graph = [] #double arrows

    def __str__(self):
        return str(self.peripheral_graph) +"\n"+ str(self.central_graph)

    def __len__(self):
        return len(self.peripheral_graph)
    

    def peripheral_neighbors(self, vertex):
        """
        Get the neighbors of a vertex in a graph.
        Parameters
        """
        return self.peripheral_graph[vertex]
    
    def tag(self,vertex):
        """
        get the tag of a vertex (the number of its peripheral neighboors
        """
        return len(self.peripheral_graph)
    
    def total_fusion_number(self):
        res = (sum([len(x) for x in self.peripheral_graph]) + len(self.central_graph))/2
        res -= sum ([1 for x in self.peripheral_graph if len(x) == 1])
        return res

    def find_nonoverlapping_bcss(self):
        """
        Find a maximum set of bipartitely-complete subgraphs (BCSs) that do not
        share any vertices.

        A maximum set means that it cannot be enlarged by adding another BCS.
        The obtained set may vary each time the function is called since the
        iterations of vertices are randomized by numpy.rdef total_fusion_number(self):
        res = (sum([len(x) for x in self.peripheral_graph]) + len(self.central_graph))/2
        res -= sum ([1 for x in self.peripheral_graph if len(x) == 1])
        return res

        get_name : bool (default: False)
            Whether to get the names or indices of vertices.

        Returns
                    if len(part1) == 1:
                        continue

                    marked = False
                    for part1_vid in part1:
                        if part1_vid in vids_in_bcs:
                            marked = True
                            break
        -------
        bcss : list of 2-tuple of list of {int or str}
            Each element corresponds to a BCS found and has the structure of
            ([v1, v2, ...], [u1, u2, ...]), where v1, v2, ... are the
            indices/names of the vertices in one part of the BCS and
            u1, u2, ... are the indices/names of the vertices in another part.
        """


        """
        MIT License

        Copyright (c) 2023 Seok-Hyung Lee

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.
        """
    
        bcss = []

        vids_in_bcs = set()  # Vertices that are already contained in a bcs
        edges_checked = set()  # Edges that are not contained in any bcs

        vids = np.arange(len(self.peripheral_graph))
        np.random.shuffle(vids)

        for vid in vids:
            if vid in vids_in_bcs:
                continue

            ngh_vids = self.peripheral_neighbors(vid)
            np.random.shuffle(ngh_vids)

            for ngh_vid1 in ngh_vids:
                if ngh_vid1 in vids_in_bcs or (vid, ngh_vid1) in edges_checked:
                    continue

                for ngh_vid2 in ngh_vids:
                    if ngh_vid1 >= ngh_vid2 or ngh_vid2 in vids_in_bcs or (
                            vid, ngh_vid2) in edges_checked:
                        continue

                    part1 = set(self.peripheral_neighbors(ngh_vid1)) & set(self.peripheral_neighbors(ngh_vid2))

                    if len(part1) == 1:
                        continue

                    marked = False
                    for part1_vid in part1:
                        if part1_vid in vids_in_bcs:
                            marked = True
                            break
                    if marked:
                        #print("[error] one vertex has elready been treated")
                        continue

                    part1_neighbors = [set(self.peripheral_neighbors(vid)) for vid in part1]
                    part2 = set.intersection(*part1_neighbors)

                    marked = False
                    for part2_vid in part2:
                        if part2_vid in vids_in_bcs:
                            marked = True
                            break
                    if marked:
                        #print("[error] one vertex has elready been treated")
                        continue
                    
                    bcss.append((part1, part2))

                    vids_in_bcs.update(part1)
                    vids_in_bcs.update(part2)

            for ngh_vid in ngh_vids:
                edges_checked.update({(vid, ngh_vid), (ngh_vid, vid)})

        return bcss

    def transformation_step(self):
        """
        performs a tranformation step on the graph : 
        it detects a complete-bipartite subgraph and makes the better simplification if the number of fusions may decrease
        The function returns True iif a moification has been made
        """

        #-----------detection of candidates for REDUCTION--------------
        bcss = self.find_nonoverlapping_bcss()
        candidates = []
        for part1,part2 in bcss :
            n = len(part1)
            m = len(part2)
            if n > 1 and m > 1 :
                n_ones = len([x for x in part1 if self.tag(x) - (m-1) == 1])
                m_ones = len([x for x in part2 if self.tag(x) - (n-1) == 1])
                candidates.append(m*n - ((n - n_ones)+(m - m_ones)) + 1)
                #print("last candidate : ",candidates[-1])
            
        maximum = 0 #so that we do not tolerate "gains" smaller than 0
        i_max = None
        for i in range(len(candidates)):
            if candidates[i] > maximum :
                i_max = i
                maximum = candidates[i]


        #-----------detection of candidates for DEVELOPMENT--------------
        candidates_development = []
        for s1,s2 in self.central_graph:
            n = self.tag(s1)
            m = self.tag(s2)
            n_ones = len([x for x in self.peripheral_neighbors(s1) if self.tag(x) == 1])
            m_ones = len([x for x in self.peripheral_neighbors(s2) if self.tag(x) == 1])
            candidates_development.append(-(m*n - ((n - n_ones)+(m - m_ones)) + 1))

        development_is_better = False
        for i in range(len(candidates_development)):
            if candidates_development[i] > maximum :
                development_is_better = True
                i_max = i
                maximum = candidates[i]

        if i_max == None :
            return 0

        if development_is_better :
            #print("[development step]")
            s1,s2 = self.central_graph[i_max]
            
            neighbours1 = [x for x in self.peripheral_neighbors(s1)]
            neighbours2 = [x for x in self.peripheral_neighbors(s2)]

            #suppression of the old links
            for x1 in self.peripheral_neighbors(s1) :
                self.peripheral_neighbors(x1).remove(s1)
            for x2 in self.peripheral_neighbors(s2):
                self.peripheral_neighbors(x2).remove(s2)

            #creation of the new links       
            for x1 in neighbours1 :
                for x2 in neighbours2 :
                    self.peripheral_neighbors(x1).append(x2)
                    self.peripheral_neighbors(x2).append(x1)

            #destruction of the double arrow
            self.central_graph.pop(i)
            return candidates_development[i_max]#suppression of the old links
            #print("mofification of the graph ... ")
            #modification of the graph
        
        else :
            #print("[reduction step]")
            n = len(self)
            self.peripheral_graph = self.peripheral_graph + [[],[]]
            self.central_graph.append((n,n+1))
                
            part1, part2 = bcss[i_max]
            #suppression of the old links
            for x1 in part1 :
                for x2 in part2 :
                    self.peripheral_neighbors(x1).remove(x2)
                    self.peripheral_neighbors(x2).remove(x1)
                
            #creation of the new links
            for x1 in part1 :
                self.peripheral_neighbors(x1).append(n)
                self.peripheral_neighbors(n).append(x1)
            
            for x2 in part2 :
                self.peripheral_neighbors(x2).append(n+1)
                self.peripheral_neighbors(n+1).append(x2)

        
            return candidates[i_max]
    

    def multiple_transformations(self):
        fusion_improvement = 0
        gain = self.transformation_step()
        print(self.total_fusion_number())
        while gain > 0 :
            fusion_improvement += gain
            gain = self.transformation_step()
            print(self.total_fusion_number())
        return fusion_improvement





if __name__ == "__main__" :
    X = []
    Y = []
    for nb_vertex in range(10,200):
        X.append(nb_vertex)
        print("treating number of vertex ", nb_vertex)
        graph, _ = constructiongraphe.random_graph_generator(nb_vertex)
        
        graphstate = Graph(graph)
        #each list is the list of neighbours of the qubit having as index the position of the list in the list of lists
        original_number_of_fustions =  graphstate.total_fusion_number()
        #print("old graphstate : \n", graphstate)
        #print("bcss : ",graphstate.find_nonoverlapping_bcss())
        print("nombre de fusions gagnées : ", graphstate.multiple_transformations())
        #print("new graphstate : \n",graphstate)
        new_number_of_fusions = graphstate.total_fusion_number()
        Y.append(100*(original_number_of_fustions - new_number_of_fusions)/original_number_of_fustions)
        
    plt.plot(X,Y)
    plt.show()

