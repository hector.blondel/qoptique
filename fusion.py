import perceval as pcvl
import perceval.components.unitary_components as comp
import numpy as np
import math
import sympy as sp
import random as rand
import matplotlib.pyplot as plt

def cluster_state_pseudo(n):
    if n==2 :
        return [[1,"00"],[1,"01"],[1,"10"],[-1,"11"]]
    cs,cs1=cluster_state_pseudo(n-1),cluster_state_pseudo(n-1)
    
    L=cs+cs1
    for i in range(2**(n-1)):
        L[i][1]+="0"
    for i in range(2**(n-1),2**n):
        if L[i][1][-1]=="1":
            L[i][0]*=-1
        L[i][1]+="1"
    return L

def cluster_state(n): # n>=2
    if n==0:
        return 1
    if n==1:
        return pcvl.StateVector([1])
    s=1/np.sqrt(2)**n
    h=pcvl.StateVector([1,0])
    v=pcvl.StateVector([0,1])
    L=cluster_state_pseudo(n)
    c_state=h**n
    for i in range(1,2**n):
        if L[i][1][0]=="0":
            psi=h
        else:
            psi=v
        for j in range(1,n):
            if L[i][1][j]=="0":
                psi*=h
            else: 
                psi*=v
        if L[i][0]==1:
            c_state+=psi
        else :
            c_state-=psi
    return c_state

def all_state(n):
    if n==2:
        return [[1,0,1,0],[1,0,0,1],[0,1,1,0],[0,1,0,1]]
    L=all_state(n-1)+all_state(n-1)
    for i in range(2**(n-1)):
        L[i]=[1,0]+L[i]
        L[i+2**(n-1)]=[0,1]+L[i+2**(n-1)]
    return L

def all_basic_state(n):
    L=all_state(n)
    S=[]
    for i in range(len(L)):
        S.append(pcvl.BasicState(L[i]))
    return S

def cs_n_circuit(n): 
    circuit=pcvl.Circuit(2*n,name='CSn')
    for i in range(0,2*n,2):
        circuit.add((i,i+1),comp.BS.H())
    for i in range(0,2*n-2,2):
        circuit.add(i,comp.PERM([0,3,2,1]))
        circuit.add((i+2,i+3),comp.BS.H())
    return circuit

def classic_state(n):
    L=[0]*(2*n)
    for i in range(n):
        L[2*i]=1
    return L

def cs_n_state(n):
    circuit=cs_n_circuit(n)
    backend = pcvl.BackendFactory().get_backend("SLOS")
    simulator = backend(circuit)
    output_state=simulator.evolve(pcvl.BasicState(classic_state(n)))
    return output_state

def postprocess_condition(o,n):
    for i in range(n):
        if o[2*i]+o[2*i +1]!=1:
            return False
    return True

def cs_n_process(n):
    processor=pcvl.Processor("SLOS",cs_n_circuit(n))
    processor.set_postprocess(lambda o: postprocess_condition(o,n))
    ca=pcvl.algorithm.Analyzer(processor,[pcvl.BasicState(classic_state(n))],all_basic_state(n))
    pcvl.pdisplay(ca)
    return processor

def measure(state,mode):
    map_measure_state = state.measure(mode)
    n=len(map_measure_state)
    i=rand.randint(0,n-1)
    weights=[]
    for s,(p,v) in map_measure_state.items():
        weights.append(p)
    #print(weights)
    L=rand.choices([k for k in range(n)],weights)
    i=L[0]
    #print(i)
    j=0
    for s,(p,v) in map_measure_state.items():
        if j==i:
            return s,p,v
        j+=1

def measure_fusion(state,i,j):
    s,p,v=measure(state,(2*i,2*i+1,2*j,2*j+1))
    #print(s,p,v)
    if s[0]+s[1]>=2 or s[2]+s[3]>=2 :
        return "fusion has failed"
    return v

def typeIIcircuit():
    circuit=pcvl.Circuit(4,name="fusion gate")
    circuit. add((0,1),comp.BS.H())
    circuit. add((0,1,2,3),comp.PERM([0,3,2,1]))
    circuit. add((0,1),comp.BS.H())
    circuit. add((2,3),comp.BS.H())
    return circuit

def typeIIgate(M,i,j): # M is number of modes i and j qbits we want to apply the fusion to
    Mi0,Mi1,Mj0,Mj1=2*i,2*i+1,2*j,2*j+1 # Modes of qbits i and j
    circuit=pcvl.Circuit(M,name="fusion gate")
    if j>i+1: # if j and i are not neighbors, make sure it is the case
        t1,L2=tuple([k for k in range(Mi1+1,Mj1+1)]),[k for k in range(Mj1-Mi1)]
        #print(t1)
        L2[0],L2[1],L2[-2],L2[-1]=L2[-2],L2[-1],L2[0],L2[1]
        #print(L2)
        circuit.add(t1,comp.PERM(L2))
    circuit. add((Mi0,Mi1,Mi1+1,Mi1+2),typeIIcircuit(),merge=False)
    if j>i+1: # put back the modes to normal
        circuit.add(t1,comp.PERM(L2))
    return circuit

def fusion_gate(input_state,i,j): # 0<=i<j<=N-1
    M=input_state.m # M modes
    circuit=typeIIgate(M,i,j)
    backend = pcvl.BackendFactory().get_backend("SLOS")
    simulator = backend(circuit)
    output_state=simulator.evolve(input_state)
    #print(output_state)
    
    return measure_fusion(output_state,i,j)

